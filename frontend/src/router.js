import Vue from 'vue'
import Router from 'vue-router'
import middleware from './middlewares'
import { auth, guest } from './middlewares/check-auth'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      // beforeEnter: middleware([guest]),
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      // beforeEnter: middleware([guest]),
    },
  ]
})
