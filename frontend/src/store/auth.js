import Api from '@/libs/api'
import localForage from 'localforage'
import { get } from 'lodash';

export default {
  namespaced: true,
  state: {
    user: null,
    token: null,
    error: null,
    loading: false,
  },
  getters: {
    isLoggedIn: (state) => !! state.user,
    currentUser: (state) => state.user,
    token: (state) => state.token,
    loading: (state) => state.loading,
    error: (state) => state.error,
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    setToken(state, token) {
      state.token = token
    },
    clearUser(state) {
      state.user = false
    },
    clearToken(state) {
      state.token = false
    }
  },
  actions: {

    async fetchUser({state, commit}) {
      state.error = null
      state.loading = true
      try {
        let response = await Api.get('/api/auth/user')
        commit('setUser', response.data.data)
      } catch (error) {
        state.error = error.message
        throw error
      } finally {
        state.loading = false
      }
    },

    async fetchToken({state, commit}, credentials) {
      state.error = null
      state.loading = true
      try {
        let response = await Api.post('/api/auth/login', credentials)
        let token = response.data.data
        commit('setToken', token)
        await localForage.setItem('token', token)
      } catch (error) {
        state.error = get(error, 'response.data.errors', error.message)
        throw error
      } finally {
        state.loading = false
      }
    },

    async login({dispatch}, credentials) {
      await dispatch('fetchToken', credentials)
      await dispatch('fetchUser')
    },

    async register({state, commit, dispatch}, userData) {
      state.error = null
      state.loading = true
      try {
        let response = await Api.post('/api/auth/register', userData)
        let token = response.data.data
        commit('setToken', token)
        await localForage.setItem('token', token)
        await dispatch('fetchUser')
      } catch (error) {
        state.error = get(error, 'response.data.errors', error.message)
        throw error
      } finally {
        state.loading = false
      }
    },

    async logout({state, commit}) {
      state.error = null
      state.loading = true
      try {
        await Api.post('/api/auth/logout')
        await localForage.removeItem('token')
        commit('clearUser')
        commit('clearToken')
      } catch (error) {
        state.error = error.message
        throw error
      } finally {
        state.loading = false
      }
    },

    async initialize({commit, dispatch}) {
      await new Promise(async (resolve) => {
        let token = await localForage.getItem('token')
        if (token) {
          commit('setToken', token)
          try {
            await dispatch('fetchUser')
          } catch (error) {
            await localForage.removeItem('token')
          }
        }
        resolve()
      })
    }

  }
}