import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { redirect } from './libs/helpers'
import api from './libs/api'
import _ from 'lodash'

import '@/../node_modules/bulma/css/bulma.css'
import '@/../node_modules/@fortawesome/fontawesome-free/scss/fontawesome.scss'


Vue.prototype._ = window._ = _
Vue.prototype.console = console
Vue.prototype.$redirect = redirect
Vue.prototype.$api = api
// test
Vue.config.productionTip = false

store.dispatch('auth/initialize').then(() => {

  window.vm = new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')

})