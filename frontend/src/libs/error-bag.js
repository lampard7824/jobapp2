let flatten = require('flat')

export default class ErrorBag {

  constructor(errors = {}) {
    this.fill(errors)
  }

  fill(errors) {
    this.errors = flatten.unflatten(errors, { object: true });
  }

  any() {
    return Object.keys(this.errors).length > 0
  }

  has(field) {
    return _.has(this.errors, field)
  }

  get(field) {
    return field ? _.get(this.errors, field) : this.errors
  }

  first(field) {
    let errors = this.get(field)
    if (errors) {
        return errors instanceof Array ? _.first(errors) : _.values(flatten(errors)).join('\n')
    }
  }

  clear(field) {
    if (field) {
      this.errors = _.omit(this.errors, field);
      return
    }
    this.errors = {};
  }

  toString() {
    return JSON.stringify(this.errors)
  }

}
