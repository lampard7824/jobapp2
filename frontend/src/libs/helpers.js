import router from '@/router'

export function redirect(location) {
  if (!location) return
  if (location === true) return window.location.reload()
  return router.push(location)
}
