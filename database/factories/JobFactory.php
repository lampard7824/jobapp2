<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Job::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->paragraph,
        'location' => $faker->word,
        'status' => $faker->randomElement(['OPEN', 'CLOSED']),
    ];
});
