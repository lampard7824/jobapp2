<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::name('api.')->namespace('Api')->group(function () {

    Route::resource('jobs', 'JobController')->only('index', 'show');

    Route::group([], function () {
        Route::post('auth/register', 'AuthController@register');
        Route::post('auth/login', 'AuthController@login');
        Route::post('auth/refresh', 'AuthController@refresh');
    });

    Route::middleware('auth:api,web')->group(function () {
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');


        Route::resource('jobs', 'JobController')->only('store', 'update', 'destroy');
    });
    
});
