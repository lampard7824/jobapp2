<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return ($request->expectsJson() || $request->is('api/*'))
                ? response()->json(['status' => 'invalid', 'message' => trans('auth.already_authenticated')], 400)
                : redirect('/home');
        }

        return $next($request);
    }
}
