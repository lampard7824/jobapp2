<?php

namespace App\Http\Controllers\Api;

use DB;
use Lang;
use Hash;
use Cookie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Validation\UnauthorizedException;

class AuthController extends Controller
{
    const REFRESH_TOKEN_COOKIE = 'refresh_token';

    use ThrottlesLogins;

    protected function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $tokenResponse = $this->requestToken('password', [
            'username' => $request->{$this->username()},
            'password' => $request->password,
        ]);
        
        if ($tokenResponse->isSuccessful()) {
            $this->clearLoginAttempts($request);
            return $this->sendSuccessResponse($tokenResponse);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function requestToken($grantType, $data)
    {
        $tokenRequest = Request::create('/oauth/token', 'POST', array_merge($data, [
            'grant_type' => $grantType,
            'client_id' => config('spa.client_id'),
            'client_secret' => config('spa.client_secret'),
        ]));
        return app()->handle($tokenRequest);
    }

    protected function sendSuccessResponse(Response $tokenResponse)
    {
        $token = json_decode($tokenResponse->getContent());

        return $this->success()
            ->withData([
                'access_token' => $token->access_token,
                'expires_in' => $token->expires_in,
            ])
            ->cookie(
                static::REFRESH_TOKEN_COOKIE, 
                $token->refresh_token, 
                10 * 24 * 60,
                "",
                "",
                true,
                true
            );
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        throw ValidationException::withMessages([
            $this->username() => [Lang::get('auth.throttle', ['seconds' => $seconds])],
        ])->status(429);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function user(Request $request)
    {
        return $this->success()->withData($request->user());
    }

    public function refresh(Request $request)
    {
        if ($refreshToken = $request->cookie(static::REFRESH_TOKEN_COOKIE)) {
            $tokenResponse = $this->requestToken('refresh_token', ['refresh_token' => $refreshToken]);
            if ($tokenResponse->isSuccessful()) {
                return $this->sendSuccessResponse($tokenResponse);
            }
        }
        throw new AuthenticationException(trans('auth.invalid_refresh'));
    }

    public function logout(Request $request)
    {
        $accessToken = $request->user()->token();
        
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        return $this->success()->cookie(Cookie::forget(static::REFRESH_TOKEN_COOKIE));
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::forceCreate([
            'name' => $request->input('email'),
            'email' => $request->input('email'),
            'email_verified_at' => now(),
            'password' => Hash::make($request->input('password')),
            'remember_token' => str_random(10),
        ]);

        $tokenResponse = $this->requestToken('password', [
            'username' => $user->{$this->username()},
            'password' => $request->password,
        ]);
        
        if ($tokenResponse->isSuccessful()) {
            return $this->sendSuccessResponse($tokenResponse);
        }

        return $this->sendFailedLoginResponse($request);

    }
}