<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\JobRepository;
use Pointless\Restful\Http\RestfulController;

class JobController extends RestfulController
{

    protected function repository(Request $request)
    {
        return new JobRepository;
    }

    public function index(Request $request)
    {
        return $this->success()->withData(
            $this->repository->paginate()
        );
    }

    public function show(Request $request, $id)
    {
        return $this->success()->withData(
            $this->repository->find($id)
        );
    }

    public function store(Request $request)
    {
        $inputs = $request->validate([
            'title' => 'required|max:200',
            'location' => 'required|max:200',
            'description' => 'required|max:1000',
            'status' => 'required|in:OPEN,CLOSED',
        ]);

        $job = $this->repository->create($inputs);

        return $this->success()->withData($job);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->validate([
            'title' => 'required|max:200',
            'location' => 'required|max:200',
            'description' => 'required|max:1000',
            'status' => 'required|in:OPEN,CLOSED',
        ]);

        $job = $this->repository->update($id, $inputs);

        return $this->success()->withData($job);
    }
    
    public function destroy(Request $request, $id)
    {
        $this->repository->delete($id);

        return $this->success();
    }
}