<?php

namespace App\Repositories;

use App\Models\Job;
use Illuminate\Database\Eloquent\Builder;
use Pointless\Repositories\EloquentRepository;

class JobRepository extends EloquentRepository
{

    public function __construct()
    {
        $this->query = Job::query();
    }

    public function create(array $inputs)
    {
        return $this->query->create($inputs);
    }

    public function update($id, array $inputs)
    {
        return tap($this->find($id))->update($inputs);
    }

    public function delete($id)
    {
        return tap($this->find($id))->delete();
    }
}