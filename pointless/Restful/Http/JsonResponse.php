<?php

namespace Pointless\Restful\Http;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse as BaseJsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Symfony\Component\HttpKernel\Exception\HttpException;

class JsonResponse extends BaseJsonResponse
{

    public static function success($message = null, $data = null, $statusCode = 200)
    {
        return (new static([
            'status' => 'success',
            'message' => $message,
        ], $statusCode))->withData($data);
    }

    public static function fail($message = null, $data = null, $statusCode = 412)
    {
        return (new static([
            'status' => 'fail',
            'message' => $message,
        ], $statusCode))->withData($data);
    }

    public static function error(Exception $exception)
    {
        if ($exception instanceof HttpException) {
            return new static(
                static::convertExceptionToArray($exception),
                $exception->getStatusCode(), 
                $exception->getHeaders(), 
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
            );
        } else {
            return new static(
                static::convertExceptionToArray($exception),
                500, 
                [],
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
            );
        }
    }

    protected static function convertExceptionToArray(Exception $e)
    {
        return config('app.debug') ? [
            'status' => 'error',
            'message' => $e->getMessage(),
            'data' => [
                'exception' => get_class($e),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => collect($e->getTrace())->map(function ($trace) {
                    return Arr::except($trace, ['args']);
                })->all(),
            ]
        ] : [
            'status' => 'error',
            'message' => ($e instanceof HttpException) ? $e->getMessage() : 'Server Error',
        ];
    }

    public static function invalid(ValidationException $exception, $message = null)
    {
        return new static([
            'status' => 'invalid',
            'message' => $message ?? $exception->getMessage(),
            'errors' => $exception->errors(),
        ], $exception->status);
    }

    public function withData($data)
    {
        if ($data instanceof Paginator) {
            $data = Resource::collection($data);
        }
        if (($data instanceof Resource) || ($data instanceof ResourceCollection)) {
            $existing = array_except((array) $this->getData(), 'data');
            $response = $data->additional($existing)->response();
            return tap($this)->setData($response->getData());
        }
        $result = $this->getData();
        $result->data = $data;
        return tap($this)->setData($result);
    }

    public function withMeta($key, $value = null)
    {
        $result = $this->getData();
        if (! is_array($key)) $key = [$key => $value];
        $result->meta = array_merge((array) object_get($result, 'meta'), $key);
        return tap($this)->setData($result);
    }

}