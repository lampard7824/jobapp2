<?php

namespace Pointless\Repositories;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Pointless\Repositories\Supports\RawFilterWhereClause;

class EloquentRepository extends Repository
{
    protected $query;

    public function __construct(Builder $query)
    {
        $this->query = $query;
    }

    public function query(Closure $clause = null)
    {
        if ($clause) {
            $clause($this->query);
            return $this;
        }
        return $this->query;
    }

    protected function filter($attribute, $filter)
    {
        (new RawFilterWhereClause($this->query))->apply($attribute, $filter);
    }

    protected function sort($attribute, $direction)
    {
        $this->query->orderBy($attribute, $direction);
    }

    public function paginate($perPage = 15, $page = null)
    {
        return $this->query->paginate($perPage, ['*'], 'page', $page);
    }

    public function all()
    {
        return $this->query->get();
    }

    public function find($key, $keyName = null)
    {
        return $keyName ? 
                $this->query->where($keyName, $key)->firstOrFail() : 
                $this->query->findOrFail($key);
    }

}