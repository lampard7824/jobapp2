<?php

namespace Pointless\Repositories;

abstract class Repository
{
    abstract public function paginate($perPage = 15, $page = null);
    
    abstract public function all();

    abstract public function find($key, $keyName = null);
    
    abstract protected function filter($attribute, $filter);

    abstract protected function sort($attribute, $direction);

    public function applyFilter($attribute, $filter)
    {
        $customFilterMethod = 'filter' . studly_case(str_replace('.', '_', $attribute));
        if (method_exists($this, $customFilterMethod)) {
            $this->$customFilterMethod($filter);
        } else {
            $this->filter($attribute, $filter);
        }
    }

    public function applySort($attribute, $direction)
    {
        $customSortMethod = 'sort' . studly_case(str_replace('.', '_', $attribute));
        if (method_exists($this, $customSortMethod)) {
            $this->$customSortMethod($direction);
        } else {
            $this->sort($attribute, $direction);
        }
    }

}