<?php

namespace Tests\Unit;

use App\Models\Job;
use Tests\TestCase;
use App\Repositories\JobRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class JobUnitTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    
    protected $jobRepository;

    public function setUp()
    {
        parent::setUp();

        $this->jobRepository = new JobRepository;
    }

    /** @test */
    public function it_can_create_a_job()
    {
        $inputs = [
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'location' => $this->faker->word,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED']),
        ];

        $job = $this->jobRepository->create($inputs);
        $this->assertInstanceOf(Job::class, $job);
        $this->assertEquals($inputs['title'], $job->title);
        $this->assertEquals($inputs['description'], $job->description);
        $this->assertEquals($inputs['location'], $job->location);
        $this->assertEquals($inputs['status'], $job->status);
        $this->assertNotNull($job->created_at);
    }

    /** @test */
    public function it_can_update_a_job()
    {
        $job = factory(Job::class)->create();

        $inputs = [
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'location' => $this->faker->word,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED']),
        ];

        $job = $this->jobRepository->update($job->id, $inputs);

        $this->assertInstanceOf(Job::class, $job);
        $this->assertEquals($inputs['title'], $job->title);
        $this->assertEquals($inputs['description'], $job->description);
        $this->assertEquals($inputs['location'], $job->location);
        $this->assertEquals($inputs['status'], $job->status);
        $this->assertNotNull($job->updated_at);
    }
    
    /** @test */
    public function it_can_find_a_job()
    {
        $job = factory(Job::class)->create();

        $foundJob = $this->jobRepository->find($job->id);

        $this->assertInstanceOf(Job::class, $foundJob);
        $this->assertEquals($foundJob->id, $job->id);
        $this->assertEquals($foundJob->title, $job->title);
        $this->assertEquals($foundJob->description, $job->description);
        $this->assertEquals($foundJob->location, $job->location);
        $this->assertEquals($foundJob->status, $job->status);
    }
    
    /** @test */
    public function it_can_delete_a_job()
    {
        $job = factory(Job::class)->create();

        $deletedJob = $this->jobRepository->delete($job->id);

        $this->assertInstanceOf(Job::class, $deletedJob);
        $this->assertEquals($deletedJob->id, $job->id);

        $this->expectException(ModelNotFoundException::class);
        $this->jobRepository->find($job->id);
    }

    /** @test */
    public function it_can_list_jobs()
    {
        $jobs = factory(Job::class, 10)->create();

        $allJobs = $this->jobRepository->all();

        $this->assertInstanceOf(Collection::class, $allJobs);
        $this->assertEquals($jobs->count(), $allJobs->count());
    }

    /** @test */
    public function it_can_paginate_jobs()
    {
        $jobs = factory(Job::class, 50)->create();

        $paginatedJobs = $this->jobRepository->paginate(10, 2);

        $this->assertInstanceOf(Paginator::class, $paginatedJobs);
        $this->assertEquals(10, $paginatedJobs->perPage());
        $this->assertEquals(2, $paginatedJobs->currentPage());
        $this->assertCount(10, $paginatedJobs->items());
        $this->assertTrue($paginatedJobs->hasMorePages());
    }
}
