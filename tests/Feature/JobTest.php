<?php

namespace Tests\Feature;

use App\Models\Job;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class JobTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_gets_paginated_jobs()
    {
        
        factory(Job::class, 50)->create();

        $response = $this->json('GET', '/api/jobs', ['page' => 2]);

        $response->assertOk()
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure([
                'data',
                'status',
                'message',
                'meta' => [
                    'current_page',
                    'last_page',
                    'per_page',
                ],
            ]);

        $json = $response->json();
        $this->assertTrue(is_array($json['data']));
        $this->assertCount($json['meta']['per_page'], $json['data']);
        $this->assertEquals(2, $json['meta']['current_page']);
    }

    /** @test */
    public function it_gets_a_job()
    {
        $job = factory(Job::class)->create();

        $response = $this->json('GET', "/api/jobs/{$job->id}");

        $response->assertOk()
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure([
                'data',
                'status',
                'message',
            ]);

        $json = $response->json();
        $this->assertEquals($job->id, $json['data']['id']);
        $this->assertEquals($job->title, $json['data']['title']);
    }
    
    /** @test */
    public function it_updates_a_job_if_authenticated()
    {

        $user = factory(User::class)->create();

        $job = factory(Job::class)->create();

        $response = $this->actingAs($user)->json('PUT', "/api/jobs/{$job->id}", $inputs = [
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'location' => $this->faker->word,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED']),
        ]);

        $response->assertOk()
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure([
                'data',
                'status',
                'message',
            ]);

        $json = $response->json();
        $this->assertEquals($job->id, $json['data']['id']);
        $this->assertEquals($inputs['title'], $json['data']['title']);
        $this->assertEquals($inputs['description'], $json['data']['description']);
        $this->assertEquals($inputs['location'], $json['data']['location']);
        $this->assertEquals($inputs['status'], $json['data']['status']);
        $this->assertNotNull($json['data']['updated_at']);
    }
    
    /** @test */
    public function it_cannot_updates_a_job_if_not_authenticated()
    {

        $job = factory(Job::class)->create();

        $response = $this->json('PUT', "/api/jobs/{$job->id}", $inputs = [
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'location' => $this->faker->word,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED']),
        ]);

        $response->assertStatus(401);
    }
    
    /** @test */
    public function it_creates_a_job_if_authenticated()
    {

        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->json('POST', "/api/jobs", $inputs = [
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'location' => $this->faker->word,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED']),
        ]);

        $response->assertOk()
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure([
                'data',
                'status',
                'message',
            ]);

        $json = $response->json();
        $this->assertEquals($inputs['title'], $json['data']['title']);
        $this->assertEquals($inputs['description'], $json['data']['description']);
        $this->assertEquals($inputs['location'], $json['data']['location']);
        $this->assertEquals($inputs['status'], $json['data']['status']);
        $this->assertNotNull($json['data']['created_at']);
    }
    
    /** @test */
    public function it_cannot_creates_a_job_if_not_authenticated()
    {

        $response = $this->json('POST', "/api/jobs", $inputs = [
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'location' => $this->faker->word,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED']),
        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function it_deletes_a_job_if_authenticated()
    {

        $user = factory(User::class)->create();

        $job = factory(Job::class)->create();

        $response = $this->actingAs($user)->json('DELETE', "/api/jobs/{$job->id}");

        $response->assertOk()
            ->assertJson(['status' => 'success']);
        
        $this->expectException(ModelNotFoundException::class);
        Job::findOrFail($job->id);
    }
    
    /** @test */
    public function it_cannot_deletes_a_job_if_not_authenticated()
    {

        $job = factory(Job::class)->create();

        $response = $this->json('DELETE', "/api/jobs/{$job->id}");

        $response->assertStatus(401);
    }
    
}
